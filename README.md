# unmannedLiveBroadcast

#### 介绍
无人直播解决方案

#### 应用场景

多平台OBS推流直播

#### 软件架构
在linux-centOS系统中,安装FFmpeg

#### 安装教程

- 安装流程

  - 前提准备

    ```
    # 需要安装重要gcc cc cl编译安装依赖
    yum -y install gcc  cc  cl
    ```

    

  - 安装yasm插件依赖

    - yasm是一款汇编器，并且是完全重写了nasm的汇编环境，接收nasm和gas语法，支持x86和amd64指令集

    ```shell
    # 下载 
    wget http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
    # 当然也可以到通过git 克隆
    git clone https://git.ffmpeg.org/ffmpeg.git
    # 解压
    tar -zxvf yasm-1.3.0.tar.gz
    # 进入目录执行安装
    cd yasm-1.3.0/
    # 生成C中间文件
    ./configure 
    # 执行安装
    make && make install
    ```

  - 安装FFmpeg

    ```
    # 下载 https://ffmpeg.org 官网
    wget http://www.ffmpeg.org/releases/ffmpeg-4.3.tar.gz
    # 解压
    tar -zxvf ffmpeg-4.3.tar.gz
    # 进入目录并执行安装
    cd ffmpeg-4.3/
    # 生成C中间文件
    ./configure 
    # 执行安装
    make && make install
    ```

  - 检查安装

    ```shell
    # 没有错误说明正常安装
    ffmpeg -version
    ```

  - 基本使用

    - 到官网参看具体的文档

    - 个别命令说明

      ```shell
      # 将一种视频格式文件转另外一种格式文件
      ffmpeg -i input.mp4 output.avi
      # 将 input.avi视频 与 音频input.mp3 合并成 output.avi
      ffmpeg -i input.avi -i input.mp3 -vcodec copy -acodec copy output.avi
      # -i 输入文件
      # -vcodec copy 表示 force video codec (‘copy’ to copy stream) 视频拷贝
      # -acodec copy 音频拷贝
      # -an:表示 disable audio 估计是audio no 之类的缩写,表示去掉video.avi 原有的音频
      # ffmpeg  -i "concat:test1.mp3|test2.mp3|test3.mp3" -c copy outputComposed.mp3 (合成音频，test1的尾+test2的首 首尾拼接式合成)
      ```

#### 使用说明

 - 直播命令

   ```linux
   ffmpeg -i 粘贴直播源 -c:v copy -c:a aac -b:a 320k -ar 44100 -strict -2 -f flv "粘贴你的直播间直播码"
   ```

 - 后台命令

   ```linux
   nohup 上边整个复制进来后台播放 &
   ```

- 测试实例

  ```
  ffmpeg -i http://tx2play1.douyucdn.cn/live/288016rlols5_1200p.flv -c:v copy -c:a aac -b:a 320k -ar 44100 -strict -2 -f flv "rtmp://alrtmpup.cdn.zhanqi.tv/zqlive/302607_7IX0O?k=70df8a61c4c1506a6a829ff36eea332c&t=5fe6e34b"
  ```

- 后台测试实例

  ```
  nohup ffmpeg -i http://tx2play1.douyucdn.cn/live/288016rlols5_1200p.flv -c:v copy -c:a aac -b:a 320k -ar 44100 -strict -2 -f flv "rtmp://alrtmpup.cdn.zhanqi.tv/zqlive/302607_7IX0O?k=70df8a61c4c1506a6a829ff36eea332c&t=5fe6e34b" &
  ```

#### 参与贡献

1.  博客  https://www.592850.com/archives/1028.html
3.  FFmpeg文档  https://ffmpeg.org/documentation.html

#### 后期展望

>使用docker 完成安装了配置
>
>直接配置即可使用